﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VisionGame
{
	[ExecuteInEditMode]
	public class ViewableByOrb : MonoBehaviour
	{
		public int timesToggledOn;
		public int timesToggledOff;
		public bool exists;
		public GameObject go;
		public Transform trs;
		public GameObject visualizerGo;
		public Transform visualizerTrs;

		void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				exists = go.activeSelf;
				return;
			}
#endif
		}

		void OnValidate ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			go.SetActive(exists);
			visualizerGo.SetActive(!exists);
		}
	}
}