using UnityEngine;
using Extensions;

namespace VisionGame
{
	public class MeshPhysicsObject : PhysicsObject
	{
#if UNITY_EDITOR
		public MeshFilter meshFilter;
		public MeshFilter visualizerMeshFilter;
		public bool updateMeshFilterMesh;
		public bool updateMeshColliderMesh;
#endif

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				MeshCollider meshCollider;
				if (updateMeshFilterMesh)
				{
					meshCollider = gameObject.AddComponent<MeshCollider>();
					meshFilter.sharedMesh = meshCollider.sharedMesh;
					visualizerMeshFilter.sharedMesh = meshCollider.sharedMesh;
					DestroyImmediate(meshCollider);
				}
				if (updateMeshColliderMesh)
				{
					meshCollider = collider as MeshCollider;
					if (meshCollider != null)
						meshCollider.sharedMesh = meshFilter.sharedMesh;
					meshCollider = visualizerCollider as MeshCollider;
					if (meshCollider != null)
						meshCollider.sharedMesh = visualizerMeshFilter.sharedMesh;
				}
			}
#endif
			base.OnEnable ();
		}
	}
}