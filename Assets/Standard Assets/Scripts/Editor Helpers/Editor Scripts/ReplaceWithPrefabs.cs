#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using Extensions;
using VisionGame;
using UnityEngine.ProBuilder;

public class ReplaceWithPrefabs : EditorScript
{
	public Transform[] replace;
	public Transform prefab;
	public bool copyMesh;

	public override void Do ()
	{
		for (int i = 0; i < replace.Length; i ++)
		{
			Transform trs = replace[i];
			// Transform clone = PrefabUtilityExtensions.ClonePrefabInstance(prefab.gameObject).GetComponent<Transform>();
			Transform clone = (Transform) PrefabUtility.InstantiatePrefab(prefab);
			clone.position = trs.position;
			clone.rotation = trs.rotation;
			clone.SetParent(trs.parent);
			clone.localScale = trs.localScale;
			if (copyMesh)
			{
				MeshFilter meshFilter = trs.GetComponent<MeshFilter>();
				if (meshFilter != null)
				{
					MeshFilter cloneMeshFilter = clone.GetComponent<MeshFilter>();
					if (cloneMeshFilter != null)
					{
						bool alreadyCopiedMesh = false;
						ProBuilderMesh proBuilderMesh = trs.GetComponent<ProBuilderMesh>();
						if (proBuilderMesh != null)
						{
							ProBuilderMesh cloneProBuilderMesh = clone.GetComponent<ProBuilderMesh>();
							if (cloneProBuilderMesh != null)
							{
								cloneProBuilderMesh.faces = proBuilderMesh.faces;
								cloneProBuilderMesh.sharedVertices = proBuilderMesh.sharedVertices;
								cloneProBuilderMesh.positions = proBuilderMesh.positions;
								cloneProBuilderMesh.ToMesh();
								cloneProBuilderMesh.Refresh();
								alreadyCopiedMesh = true;
							}
						}
						if (!alreadyCopiedMesh)
							cloneMeshFilter.mesh = meshFilter.mesh;
					}
				}
			}
			GameManager.DestroyOnNextEditorUpdate (trs.gameObject);
		}
		replace = new Transform[0];
	}
}
#else
public class ReplaceWithPrefabs : EditorScript
{
}
#endif