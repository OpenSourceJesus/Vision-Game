using UnityEngine;

namespace VisionGame
{
	public class PinkZone : MonoBehaviour
	{
		void OnTriggerEnter (Collider other)
		{
			Orb orb = other.GetComponent<Orb>();
			if (orb != null)
				orb.enabled = false;
		}

		void OnTriggerExit (Collider other)
		{
			Orb orb = other.GetComponent<Orb>();
			if (orb != null)
				orb.enabled = true;
		}
	}
}