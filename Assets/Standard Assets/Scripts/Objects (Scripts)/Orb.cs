﻿using UnityEngine;
using System.Collections.Generic;
using Extensions;

namespace VisionGame
{
	public class Orb : SpherePhysicsObject, IUpdatable
	{
		public bool PauseWhileUnfocused
		{
			get
			{
				return false;
			}
		}
		public Transform cameraTrs;
		public new Camera camera;
		public LayerMask opaqueLayermask;
		public LayerMask transparentLayermask;
		// public float checkDistance;
		public List<GameObject> gosIAmViewing = new List<GameObject>();
		public List<GameObject> previousGosIAmViewing = new List<GameObject>();
		// public Vector2Int checksPerViewportAxis;
		// public float[] checkDistances = new float[0];

// #if UNITY_EDITOR
// 		void OnValidate ()
// 		{
// 			checkDistances = new float[(checksPerViewportAxis.x + 1) * (checksPerViewportAxis.y + 1)];
// 			int currentIndex = 0;
// 			Plane plane = new Plane(cameraTrs.forward, cameraTrs.position + cameraTrs.forward * camera.farClipPlane);
// 			for (float x = 0; x <= 1f; x += 1f / checksPerViewportAxis.x)
// 			{
// 				for (float y = 0; y <= 1f; y += 1f / checksPerViewportAxis.y)
// 				{
// 					Vector2 viewportPoint = new Vector2(x, y);
// 					Ray viewportRay = camera.ViewportPointToRay((Vector3) viewportPoint);
// 					float hitDistance = 0;
// 					plane.Raycast(viewportRay, out hitDistance);
// 					checkDistances[currentIndex] = hitDistance;
// 					currentIndex ++;
// 				}
// 			}
// 		}
// #endif

		public override void OnEnable ()
		{
			base.OnEnable ();
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			ReplaceObjects ();
			previousGosIAmViewing = new List<GameObject>(gosIAmViewing);
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
		
		public void ReplaceObjects ()
		{
			HandleViewGameObjects ();
			for (int i = 0; i < gosIAmViewing.Count; i ++)
			{
				GameObject goIAmViewing = gosIAmViewing[i];
				ViewableByOrb viewableByOrb = goIAmViewing.GetComponentInParent<ViewableByOrb>();
				if (viewableByOrb != null && !previousGosIAmViewing.Contains(viewableByOrb.go) && !previousGosIAmViewing.Contains(viewableByOrb.visualizerGo))
				{
					viewableByOrb.timesToggledOn ++;
					if (viewableByOrb.timesToggledOn > viewableByOrb.timesToggledOff)
					{
						viewableByOrb.exists = !viewableByOrb.exists;
						viewableByOrb.go.SetActive(!viewableByOrb.go.activeSelf);
						viewableByOrb.visualizerTrs.position = viewableByOrb.trs.position;
						viewableByOrb.visualizerTrs.rotation = viewableByOrb.trs.rotation;
						viewableByOrb.visualizerTrs.localScale = viewableByOrb.trs.localScale;
						viewableByOrb.visualizerGo.SetActive(!viewableByOrb.visualizerGo.activeSelf);
					}
				}
			}
			for (int i = 0; i < previousGosIAmViewing.Count; i ++)
			{
				GameObject previousGoIAmViewing = previousGosIAmViewing[i];
				ViewableByOrb viewableByOrb = previousGoIAmViewing.GetComponentInParent<ViewableByOrb>();
				if (viewableByOrb != null && !gosIAmViewing.Contains(viewableByOrb.go) && !gosIAmViewing.Contains(viewableByOrb.visualizerGo))
				{
					viewableByOrb.timesToggledOff ++;
					if (viewableByOrb.timesToggledOn >= viewableByOrb.timesToggledOff)
					{
						viewableByOrb.exists = !viewableByOrb.exists;
						viewableByOrb.go.SetActive(!viewableByOrb.go.activeSelf);
						viewableByOrb.visualizerTrs.position = viewableByOrb.trs.position;
						viewableByOrb.visualizerTrs.rotation = viewableByOrb.trs.rotation;
						viewableByOrb.visualizerTrs.localScale = viewableByOrb.trs.localScale;
						viewableByOrb.visualizerGo.SetActive(!viewableByOrb.visualizerGo.activeSelf);
					}
				}
			}
		}

		void HandleViewGameObjects ()
		{
			gosIAmViewing.Clear();
			// int currentIndex = 0;
			// for (float x = 0; x <= 1f; x += 1f / checksPerViewportAxis.x)
			// {
			// 	for (float y = 0; y <= 1f; y += 1f / checksPerViewportAxis.y)
			// 	{
					// Vector2 viewportPoint = new Vector2(x, y);
					Vector2 viewportPoint = Vector2.one / 2;
					Ray viewportRay = camera.ViewportPointToRay((Vector3) viewportPoint);
					// float checkDistance = checkDistances[currentIndex];
					// float checkDistanceRemaining = checkDistance;
					float checkDistance = camera.farClipPlane;
					float checkDistanceRemaining = checkDistance;
					RaycastHit hit;
					GameObject goIAmViewing;
					if (Physics.Raycast(viewportRay, out hit, checkDistanceRemaining, opaqueLayermask))
					{
						goIAmViewing = hit.collider.gameObject;
						checkDistanceRemaining -= hit.distance;
						if (!gosIAmViewing.Contains(goIAmViewing))
							gosIAmViewing.Add(goIAmViewing);
						HandleViewGameObject (goIAmViewing, viewportRay.direction, hit, checkDistanceRemaining);
					}
					RaycastHit[] hits = Physics.RaycastAll(viewportRay, checkDistanceRemaining, transparentLayermask);
					for (int i = 0; i < hits.Length; i ++)
					{
						hit = hits[i];
						goIAmViewing = hit.collider.gameObject;
						if (!gosIAmViewing.Contains(goIAmViewing))
							gosIAmViewing.Add(goIAmViewing);
						HandleViewGameObject (goIAmViewing, viewportRay.direction, hit, checkDistance - hit.distance);
					}
					// currentIndex ++;
				// }
			// }
		}

		void HandleViewGameObject (GameObject go, Vector3 rayDirection, RaycastHit hit, float remainingDistance)
		{
			Mirror mirror = go.GetComponentInParent<Mirror>();
			if (mirror != null)
			{
				rayDirection = Vector3.Reflect(rayDirection, hit.normal);
				Ray ray = new Ray(hit.point, rayDirection);
				GameObject goIAmViewing;
				float previousRemainingDistance = remainingDistance;
				if (Physics.Raycast(ray, out hit, remainingDistance, opaqueLayermask))
				{
					goIAmViewing = hit.collider.gameObject;
					remainingDistance -= hit.distance;
					if (!gosIAmViewing.Contains(goIAmViewing))
						gosIAmViewing.Add(goIAmViewing);
					HandleViewGameObject (goIAmViewing, rayDirection, hit, remainingDistance);
				}
				RaycastHit[] hits = Physics.RaycastAll(ray, remainingDistance, transparentLayermask);
				for (int i = 0; i < hits.Length; i ++)
				{
					hit = hits[i];
					goIAmViewing = hit.collider.gameObject;
					if (!gosIAmViewing.Contains(goIAmViewing))
						gosIAmViewing.Add(goIAmViewing);
					HandleViewGameObject (goIAmViewing, rayDirection, hit, previousRemainingDistance - hit.distance);
				}
			}
		}
	}
}